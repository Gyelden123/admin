import React, { Fragment, useEffect, useState } from "react";
import "./User.css";

const ListUser = () => {
  const [users, setUsers] = useState([]);

  const getUsers = async () => {
    try {
      const response = await fetch("https://assetsvaultadmin.onrender.com/users");
      const jsonData = await response.json();

      setUsers(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getUsers();
  }, []);

  // delete function
  const deleteUsers = async (id) => {
    if (window.confirm("Are you sure?")) {
      try {
        await fetch(`https://assetsvaultadmin.onrender.com/users/${id}`, {
          method: "DELETE",
        });
        setUsers(users.filter((user) => user.user_id !== id));
        window.alert("User successfully deleted.");
      } catch (err) {
        console.error(err.message);
      }
    }
  };

  return (
    <Fragment>
      <div className="container mt-4">
        <h2 className="mt-3 text-light">
          <span style={{ color: "#FE4C00" }}>Users</span> Table
        </h2>
        <hr className="mt-5 text-light" />
      </div>
      <div className="container mt-4 p-3" style={{ overflow: "auto", height: "400px" }}>
        <div className="row">
          {users.length > 0 &&
            users.map((user) => (
              <div className="col-md-6 col-lg-4 mb-4" key={user.user_id}>
                <div className="card user-card">
                  {user.encode && (
                    <img
                      className="card-img-top rounded mx-auto d-block w-75"
                      src={`data:image/jpeg;base64,${user.encode}`}
                      alt=""
                    />
                  )}
                  <div className="card-body">
                    <h5 className="card-title">{user.user_name}</h5>
                    <p className="card-text">{user.user_email}</p>
                    <button
                      className="btn btn-danger"
                      onClick={() => deleteUsers(user.user_id)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </Fragment>
  );
};

export default ListUser;
