import React, { useEffect, useState } from 'react';
import { NavLink, Outlet, useLocation, useNavigate } from 'react-router-dom';
import './Drawer.css';

function Drawer() {
  const location = useLocation();
  const navigate = useNavigate();
  const [showLinks, setShowLinks] = useState(true);
  const [toggleEnabled, setToggleEnabled] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      const shouldShowLinks = window.innerWidth > 992;
      setShowLinks(shouldShowLinks);
      setToggleEnabled(!shouldShowLinks);
    };

    window.addEventListener('resize', handleResize);
    handleResize(); // Check on initial load
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const handleToggle = () => {
    setShowLinks(!showLinks);
  };

  useEffect(() => {
    // Redirect to the dashboard if the current path is '/'
    if (location.pathname === '/') {
      navigate('/admin/dashboard');
    }
  }, [location.pathname, navigate]);

  return (
    <div className="container-fluid p-5">
      <div className="row">
        <div className={`col-md-${showLinks ? '3' : '12'}`}>
          <div className="sidenav">
            <div className="sidenav-content border border-light p-3">
              <div className='text-start'>
                <h4 className='text-light pt-3'>
                  <span style={{ color: "#FE4C00" }}>Assets</span> Vault
                  {toggleEnabled && (
                    <button
                      className={`btn btn-light text-dark float-end ${showLinks ? 'active' : ''}`}
                      onClick={handleToggle}
                    >
                      {showLinks ? 'Hide' : 'Show'}
                    </button>
                  )}
                </h4>
              </div>
              {showLinks && (
                <>
                  <hr className='text-light' />
                  <NavLink
                    to="/admin"
                    className={`sidenav-item text-light ${
                      location.pathname === '/admin/dashboard' ? 'active' : ''
                    }`}
                  >
                    Dashboard
                  </NavLink>
                  <NavLink
                    to="/admin/users"
                    className={`sidenav-item text-light ${
                      location.pathname.includes('/users') ? 'active' : ''
                    }`}
                  >
                    View Users
                  </NavLink>
                  <NavLink
                    to="/admin/assets"
                    className={`sidenav-item text-light ${
                      location.pathname.includes('/assets') ? 'active' : ''
                    }`}
                  >
                    View Assets
                  </NavLink>
                </>
              )}
            </div>
          </div>
        </div>

        <div className={`col-md-${showLinks ? '9' : '12'}`}>
          <div className="content border border-light p-3">
            <Outlet />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Drawer;
