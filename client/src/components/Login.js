import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function Login() {
  const defaultEmail = "12200004.gcit@rub.edu.bt";
  const defaultPassword = "Gyelden Dorji7";
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const validateForm = () => {
    if (!email) {
      setError("Email is required");
      return false;
    }

    if (!password) {
      setError("Password is required");
      return false;
    }

    if (email !== defaultEmail || password !== defaultPassword) {
      setError("Invalid user");
      return false;
    }

    setError("");
    return true;
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (validateForm()) {
      // Perform login logic here, such as sending the email and password to a server for verification
      console.log("Email:", email);
      console.log("Password:", password);

      // Clear form fields after submission
      setEmail("");
      setPassword("");

      // Navigate to the drawer page
      navigate("/admin");
    }
  };

  return (
    <div className="container p-5">
      <div className="container">
        <h2>
          <span style={{ color: "#FE4C00" }}>Assets</span>{" "}
          <span className="text-light">Vault</span>
        </h2>
        <div className="container mt-5 w-75 rounded  bg-light p-3">
          <h4 className="text-center">Admin Login</h4>
          <hr className="mt-4" />
          {error && (
            <div className="container text-danger p-3 rounded text-center bg-danger w-50 text-light h6 border">
              {error}
            </div>
          )}
          <form className="container p-2" onSubmit={handleSubmit}>
            <div className="container">
              <input
                className="form-control"
                placeholder="Email"
                type="email"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
              />
            </div>
            <div className="container mt-4">
              <input
                className="form-control"
                placeholder="Password"
                type="password"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
              />
            </div>
            <div className="container text-center mt-4">
              <button
                className="btn w-25"
                style={{
                  backgroundColor: "#FE4C00",
                  color: "white",
                  fontSize: 20,
                  fontWeight: "500",
                }}
                type="submit"
              >
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default Login;
