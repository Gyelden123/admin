import React, { Fragment, useEffect, useState } from "react";

const ListAssets = () => {
  const [assets, setAssets] = useState([]);

  // retrieving all users
  const getAssets = async () => {
    try {
      const response = await fetch("https://assetsvaultadmin.onrender.com/assets");
      const jsonData = await response.json();

      setAssets(jsonData);
    } catch (err) {
      console.error(err.message);
    }
  };

  useEffect(() => {
    getAssets();
  }, []);

  // delete function
  const deleteAssets = async (id) => {
    console.log("asset_id :", id);
    try {
      const shouldDelete = window.confirm("Are you sure you want to delete this asset?");
      if (shouldDelete) {
        await fetch(`https://assetsvaultadmin.onrender.com/assets/${id}`, {
          method: "DELETE",
        });

        setAssets(assets.filter((asset) => asset.asset_id !== id));
        window.alert("Asset is successfully deleted");
      }
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <Fragment>
      <div className="container mt-4">
        <h2 className="mt-3 text-light">
          <span style={{ color: "#FE4C00" }}>Assets</span> Table
        </h2>
        <hr className="mt-5 text-light" />
      </div>
      <div className="container mt-4 p-3" style={{ overflow: "auto", height: "400px" }}>
        <div className="row">
          {assets.length > 0 &&
            assets.map((asset) => (
              <div className="col-md-6 col-lg-4 mb-4" key={asset.asset_id}>
                <div className="card user-card">
                  <div className="card-body">
                    <h6 className="card-title">Name: {asset.name}</h6>
                    <p className="card-text">FileSize: {asset.filesize}MB</p>
                    <p className="card-text">
                      Engine Compatibility:
                      <ul>
                        {asset.engine.map((compatibility, index) => (
                          <li key={index}>{compatibility}</li>
                        ))}
                      </ul>
                    </p>
                    <p className="card-text">Overview: {asset.overview}</p>
                    <p className="card-text">ModelType: {asset.modeltype}</p>
                    <p className="card-text">Average Rating: {asset.average_rating}</p>
                    <button
                      className="btn btn-danger"
                      onClick={() => deleteAssets(asset.asset_id)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    </Fragment>
  );
};

export default ListAssets;
