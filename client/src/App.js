import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Drawer from './components/Drawer';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import ListUser from './components/Users';
import ListAssets from './components/Assets';

function App() {
  
  return (
    <Router>
      <Routes>
        <Route path='/' element={<Login />} />
        <Route path='/admin' element={<Drawer />}>
          <Route index element={<Dashboard />} />
          <Route path='users' element={<ListUser />} />
          <Route path='assets' element={<ListAssets />} />
        </Route>
      </Routes>
    </Router>
  );
}

export default App;
